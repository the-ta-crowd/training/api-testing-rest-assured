# Assignments

> All the files which you will need to edit in these assignments are located in
>**src/test/java/nl/capgemini/testing/campgemini**

## Getting started

1. In IntelliJ, navigate to **src/main/java/nl/capgemini/testing/campgemini** and run CampGeminiApplication.Java.
2. In a Chrome browser navigate to "localhost:8080". Validate that the application is running.

## Exercise 1 - GETting acquainted with our API
1. What you see now is a simple front-end that shows the data stored in the database. Open a second tab and navigate to
   the API call at "http://localhost:8080/api/campgemini/".
2. What is the price of a "Duo Tent" according to the front-end?
3. What is the price of a "Duo Tent" according to the API?
4. Which type of API call is done in Exercise 1.1? GET, POST, PUT or DELETE?
5. On the front-end tab, inspect the url link of "Duo Tent" and "Pen-T Anchors".
   What is the difference between the urls?
6. Can you explain this difference using the API response from Exercise 1.1?
7. In the API response tab, try to filter the response to only show the "Duo Tent".
> Hint: The README.md contains more information about the API.
8. In the API response tab, try to get the item with id 42. What do you see? What does this mean?

## Exercise 2 - API Logging
1. In IntelliJ navigate to the tab "Run" on the bottom of the screen. Here you will find the logging of our application.
2. Which API calls have been made already since the start of the application?
3. In Chrome, in the API response tab, navigate to "http://localhost:8080/api/campgemini/Duo-Tent". What do you see?
4. In IntelliJ, check the logging to see the new log message created by Exercise 2.3.
   Explain the difference between this message and the one in exercise 1.8.

## Exercise 3 - GET requests
### Exercise 3.1 - Making a GET request test
1. In IntelliJ navigate to exercise_3 and open GETRequest.
> Hint: Exercises are located in **src/test/java/nl/capgemini/testing/campgemini/assignments**

Run the test GetRequestTest() in the file by pressing the play button on the left side navigation.
2. What happens? Why is that?
3. Inside the GetRequestTest() method, after `when().get(/api/campgemini/")`, add `.then().statusCode(200);`
4. Run the test again. What happens now?

### Exercise 3.2 - Making a second GET request test
Inside GETRequest, we are going to create a new test.
1. In GETRequest, make a new test called `MyOwnBadRequestTest()`.
   In this test create your own API test where you test for status 400.
> Hint: You can copy paste most of the code from `GetRequestTest()`.
2. Run the test `MyOwnBadRequestTest()`. What happens?
3. Now make the test pass for statusCode 400.
> Hint: Remember exercise 2.3

### Exercise 3.3 - GET request testing with assertions
1. In the folder exercise_3, create a new Java class with the name MyGETRequestTest.
2. In the class MyGETRequestTest create a new testcase, that checks the statusCode (200) for a 'Duo Tent'
3. Now create a new Testcase based on your own previous testcase that prints out the response for this GET request.
> Hint: Replace statuscode assertion with extract().response().prettyPrint();
4. Now create another new testcase that checks if the response *contains* the text 'Duo Tent' using 'assertTrue'.
> Hint 1: assertTrue is a part of the Java library 'Assertions'. You can put an entire 'given > when > then' structure inside an assertTrue.
> Hint 2: assertTrue needs String variables to assert things. Guess what prettyPrint()'s output is?
5. Copy your previous testcase in a new test method and now don't extract the response, but the jsonPath with:
   `jsonPath().get("KEY");` replacing KEY with the correct key.
> Hint: You've seen the key before. To check it, visit the API in your browser.

## Exercise 4 - POST requests
### Exercise 4.1 - POST request of Item
1. In the package exercise_4, open the POSTRequest java class. You have the method PostNowItem. Replace the "FIX ME"
   with the following JSON String "{\"name\": \"Overclocked Sleeping Bag\",\"price\": 99.99}" .
   Include the starting and ending quotes.
> Hint: When pasting, make sure to include the quotes as well or get ready to remove a lot of escape symbols (backslashes).
2. Add a status code check in your POST request test. Make sure the test passes. A passing test doesn't always mean a successful POST!
> Hint 1: Forgot where to put a statusCode check? See Exercise 3.1.3.
> Hint 2: Use log().all() to get more information concerning your request/response.
3. Explain the used status code.
4. Copy the working POST request from 2, and add the following ".header("role","employee")" after the given().
5. Run this test. Update the status code if necessary.
6. Execute a GET request on the newly created item. Guess which 'id' it has. What do you see?
7. Now update the GET request to count amount of items using JSONPath.
> Hint: use jsonPath().getList() instead of jsonPath().get(). You can count the items in the list by determining its *size*.
8. Create a GET request that counts with JSONPath the amount of items before sending the POSTRequest
9. Compare and assert the change in item count.

### Exercise 4.2 - POST request of Item
1. POST a new Item with a Name, Price and Description.
2. Validate that the item count has increases between the request.
3. Validate that your Name, price and description can be retrieved using a
   GET Request with validating the response content.

### Exercise 4.3 - POST request of Item with JSONReader
1. In the package exercise_4 open the class. POSTRequest_JSON.
   This class class uses a JSONFileReader to read the Test data out of json file.
   The file is  test/resources/testData.json
2. Open the testData.json and define new items.
> Hint: Remember what the KEY's were? So which ones are there and what VALUEs can they have?
3. Save the file.
   NB. The file needs to start with: `[ {` and end with `} ]`
> Hint: You can validate your JSON file using the following website `https://jsonformatter.curiousconcept.com/`
4. Open POSTRequest_JSON.java in Exercise_4.
5. The test postWithJson has already defined the Location of the JsonFile and the Reader to use.
> Hint: to retrieve an item out of our testDataList you can use "jsonTestDataList.get(number of item)"
6. Create a for-loop that POST each item sequentially.
> Hint: Use the toString() method on the item you want to POST
7. Validate that the items are created.

## Exercise 5 - PUT requests
### Exercise 5.1 - PUT request of item
1. In your class PUTRequest, Create a new test.
2. In the test write test code, to update the item of Exercise 4.1 using PUT
> Hint : See Slides PUTrequest.
3. Validate the statusCode.
3. In the same test, create a GETRequest and use JSONpath to validate the update.

Extra :
Try to create new Item but with PUT.
Try to update and existing item with POST.

## Exercise 6 - DELETE requests
### Exercise 6.1 - DELETE request single Item
1. In the class DELETERequest, Create a new test.
2. In the test, write test code, to delete the item of Exercise 4.1 using DELETE
> Hint : Use the Header to authenticate (see exercise 4.1).
3. Validate the statusCode.

### Exercise 6.2 - DELETE request all Items
1. In the class DeleteRequest, Create a new test.
2. In the test, write test code, to delete all the items in the database using a single DELETE request
3. Validate the statusCode.
4. Validate that the database is empty.





## Case study
Prerequisite:
1.  Restart the CampGeminiApplication (Press the play button in class CampGeminiApplication)
    NB. API Definition can be found in README.md

### Case:
As an employee,
I want to be able to create a set of new items.
This set need to be updated after creation with the price

As a customer
I want to get a list of all the available items below the price of 101 EURO.
And I want to buy the first available item around 50 euros.

As an angry employee
I want to delete all items from the shop.

