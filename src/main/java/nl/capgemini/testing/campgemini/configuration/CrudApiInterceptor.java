package nl.capgemini.testing.campgemini.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;

@Component
public class CrudApiInterceptor extends HandlerInterceptorAdapter {

    private static Logger LOGGER = LoggerFactory.getLogger(CrudApiInterceptor.class);

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) {
        LOGGER.info(formatRequest(request));
        return true;
    }

    private String formatRequest(HttpServletRequest request) {
        Enumeration<String > myHeaders = request.getHeaderNames();
        String logline = "\n{\"Headers\":\n {\n";
        while(myHeaders.hasMoreElements()) {
            String header = myHeaders.nextElement();
            logline += "\""+header+"\" :" +"\""+request.getHeader(header)+"\"" +"\n";

        }
        logline += "}\n";
        logline += "\"Method\": " +"\""+request.getMethod() +"\"\n}";
        return logline;
    }


}
