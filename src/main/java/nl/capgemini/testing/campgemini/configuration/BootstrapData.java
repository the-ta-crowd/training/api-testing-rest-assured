package nl.capgemini.testing.campgemini.configuration;

import nl.capgemini.testing.campgemini.database.CatalogueItem;
import nl.capgemini.testing.campgemini.database.CatalogueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class BootstrapData {

    private final CatalogueRepository catalogueRepository;

    @Autowired
    public BootstrapData(final CatalogueRepository catalogueRepository) {
        this.catalogueRepository = catalogueRepository;
    }

    @PostConstruct
    public void createCatalogue() {
        catalogueRepository.save(new CatalogueItem()
                .setName("Duo Tent")
                .setPrice(150)
                .setDescription("A tent for twins and camps, born under the astrological sign 'Gemini'.")
                .setSupply(15)
        );
        catalogueRepository.save(new CatalogueItem()
                .setName("Pen-T Anchors")
                .setPrice(7.99)
                .setDescription("Anchors for serious climbers. These sharp, titanium-enhanced iron rope anchors will " +
                        "penetrate rocks with ease and can stand the test of time.")
                .setSupply(12)
        );
        catalogueRepository.save(new CatalogueItem()
                .setName("FGC-19 Rope")
                .setPrice(100.99)
                .setDescription("Highly durable rope for climbers, which they can use to support their lives. " +
                        "WARNING: Don't become too entangled or it might consume your life.")
                .setSupply(49)
        );
        catalogueRepository.save(new CatalogueItem()
                .setName("Spike Air Hex Sprinting Shoes")
                .setPrice(119.95)
                .setDescription("Refine your your own story and increase your velocity to epic heights with these comfortable shoes.")
                .setSupply(23)
        );
    }
}
