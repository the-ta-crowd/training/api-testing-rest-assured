package nl.capgemini.testing.campgemini.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.function.Supplier;

@Service
public class AuthenticationService {

    public ResponseEntity<?> authenticateAndRun(final String userRole, final String requiredRoles,
                                                final Supplier<ResponseEntity<?>> codeToRun) {
        boolean authenticated;

        if (userRole == null) {
            authenticated = checkAuthentication("", requiredRoles);
        } else {
            authenticated = checkAuthentication(userRole, requiredRoles);
        }
        if (authenticated) {
            return codeToRun.get();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    private boolean checkAuthentication(final String userRole, final String requiredRoles) {
        return (',' + requiredRoles + ',').contains(',' + userRole + ',');
    }

}
