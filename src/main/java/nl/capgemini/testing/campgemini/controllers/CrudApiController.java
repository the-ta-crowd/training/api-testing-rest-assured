package nl.capgemini.testing.campgemini.controllers;

import nl.capgemini.testing.campgemini.database.CatalogueItem;
import nl.capgemini.testing.campgemini.services.AuthenticationService;
import nl.capgemini.testing.campgemini.services.CampGeminiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CrudApiController {

    private final AuthenticationService authenticationService;
    private final CampGeminiService campGeminiService;

    @Autowired
    public CrudApiController(final AuthenticationService authenticationService,
                             final CampGeminiService campGeminiService) {
        this.authenticationService = authenticationService;
        this.campGeminiService = campGeminiService;
    }

    @PutMapping(value = "/api/campgemini/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> createNewItem(@RequestBody final CatalogueItem newItem,
                                           @RequestHeader(value = "role", required = false) final String role) {
        return authenticationService.authenticateAndRun(role, "employee",
                () -> campGeminiService.createNewItem(newItem));
    }

    @GetMapping(value = "/api/campgemini/", produces = "application/json")
    public ResponseEntity<?> retrieveCatalogueList(
            @RequestHeader(value = "role", required = false) final String role) {
        return authenticationService.authenticateAndRun(role, "",
                campGeminiService::retrieveCatalogueList);
    }

    @GetMapping(value = "/api/campgemini/{id}", produces = "application/json")
    public ResponseEntity<?> retrieveItem(@PathVariable("id") final long id,
                                          @RequestHeader(value = "role", required = false) final String role) {
        try{
            Long.parseLong(String.valueOf(id));
        }catch(Exception e)
        {
            return ResponseEntity.badRequest().build();
        }
        return authenticationService.authenticateAndRun(role, "",
                () -> campGeminiService.retrieveItem(id));
    }

    @PostMapping(value = "/api/campgemini/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> updateExistingItem(@PathVariable("id") final long id,
                                                @RequestBody final CatalogueItem newItem,
                                                @RequestHeader(value = "role", required = false) final String role) {
        return authenticationService.authenticateAndRun(role, "employee",
                () -> campGeminiService.updateExistingItem(id, newItem));
    }

    @PostMapping(value = "/api/campgemini/{id}/buy", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> buyItem(@PathVariable("id") final long id,
                                     @RequestBody(required = false) final Integer amount,
                                     @RequestHeader(value = "role", required = false) final String role) {
        return authenticationService.authenticateAndRun(role, "",
                () -> campGeminiService.buyItem(id, amount));
    }

    @DeleteMapping(value = "/api/campgemini/{id}")
    public ResponseEntity<?> deleteItem(@PathVariable("id") final long id,
                                        @RequestHeader(value = "role", required = false) final String role) {
        return authenticationService.authenticateAndRun(role, "employee",
                () -> campGeminiService.deleteItem(id));
    }

    @DeleteMapping(value = "/api/campgemini/")
    public ResponseEntity<?> deleteAllItems(@RequestHeader(value = "role", required = false) final String role) {
        return authenticationService.authenticateAndRun(role, "employee",
                campGeminiService::deleteAllItems);
    }

}
