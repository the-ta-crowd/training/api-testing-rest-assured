package nl.capgemini.testing.campgemini.database;

import org.springframework.data.repository.CrudRepository;

public interface CatalogueRepository extends CrudRepository<CatalogueItem, Long> {
}
