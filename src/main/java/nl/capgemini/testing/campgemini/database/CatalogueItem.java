package nl.capgemini.testing.campgemini.database;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CatalogueItem {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String name ="";
    private double price = 0.0;
    private String description = "";
    private int supply = 0;

    public CatalogueItem()
    {}

    protected CatalogueItem(Long id, String name, double price, String description, int supply)
    {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.supply = supply;
    }


    public Long getId() {
        return id;
    }

    public CatalogueItem setId(final Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CatalogueItem setName(final String name) {
        this.name = name;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public CatalogueItem setPrice(final double price) {
        this.price = price;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CatalogueItem setDescription(final String description) {
        this.description = description;
        return this;
    }

    public Integer getSupply() {
        return supply;
    }

    public CatalogueItem setSupply(final Integer supply) {
        if(supply == null)
        {
            this.supply = 0;
        }else {
            this.supply = supply;
        }
        return this;
    }

    public static Builder builder(CatalogueItem item) {
        return new Builder(item);
    }

    public static class Builder{

        private Long id;

        private String name;
        private double price;
        private String description;
        private Integer supply;

        public Builder(CatalogueItem item)
        {
            this.id = item.getId();
            this.name = item.getName();
            this.price = item.getPrice();
            this.description = item.getDescription();
            this.supply = item.getSupply();
        }



        public Builder withItem(CatalogueItem item)
        {
            if (item.getDescription().equals("")) {
                this.description = item.getDescription();
            }
            if (item.getSupply() == 0) {
                this.supply = item.getSupply();
            }
            if (item.getName().equals("")) {
                this.name = item.getName();
            }
            if (item.getPrice() == 0.0) {
                this.price = item.getPrice();
            }
            return this;
        }

        public CatalogueItem build() {
            return new CatalogueItem(id, description, price, name, supply);
        }
    }
}
