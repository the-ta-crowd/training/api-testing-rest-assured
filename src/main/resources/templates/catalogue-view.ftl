<!DOCTYPE html>
<html lang="en-US">
    <head>
    <title>CampGemini</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Keywords" content="Capgemini,Testing,API-Testing,API">
    <meta name="Description" content="CampGemini application to practice API testing.">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet'>
    <!-- Bootstrap, jQuery, Popper en JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
   <body>
       <div class="jumbotron">
        <h1>CampGemini</h1>
        <p>For your life with or without IT</p>
       </div>
       <div class="row container">

            <#list catalogue as item>
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <p><h3><a href="/catalogue/${item.id}">${item.name}</a></h3></p>
                        </div>
                        <div class="card-body">
                            <h4>Price</h4>
                            <p>${item.price}</p>
                        </div>
                        <div class="card-footer">
                            <h4>Supply</h4>
                            <p>${item.supply}</p>
                        </div>
                    </div>
                </div>
            <#else>
                <div class="col">
                    <div class="card">
                    <div class="card-header">Header</div>
                    <div class="card-body"><h3>No items available.</h3></div>
                    <div class="card-footer"><h3>Please come again!</h3></div>
                    </div>
                </div>
            </#list>

       </div>
   </body>
</html>