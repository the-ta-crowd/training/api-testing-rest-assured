package nl.capgemini.testing.campgemini.assignments.exercise_4;

import com.fasterxml.jackson.databind.JsonNode;
import nl.capgemini.testing.campgemini.helpers.JSONfileReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class POSTRequestJSON {

    private JSONfileReader jsonFileReader = JSONfileReader.getInstance();
    private ArrayList<JsonNode> jsonTestDataList;

    /**
     * This method will read before each test our Testdata located in src/test/resources.testData.json
     * If you want to get the first item of our test data you can use "jsonTestDataList.get(0)";
     * jsonTestDataList.get(0).toString(); Convert the data into a String object that can be Posted to our backend.
     */
    @BeforeEach
    public void readData() throws IOException {
        String path = "src/test/resources/testData.json";
        jsonTestDataList = jsonFileReader.getJsonArray(path);
    }

    @Test
    void postWithJson() {
        // json
    }
}
