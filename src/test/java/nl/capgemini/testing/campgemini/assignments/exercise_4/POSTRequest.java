package nl.capgemini.testing.campgemini.assignments.exercise_4;

import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static io.restassured.RestAssured.given;

class POSTRequest {

    @Test void
    postNowItem()
    {
        given().
                baseUri("http://localhost:8080/api/campgemini/").
                when().
                contentType(ContentType.JSON).
                body("FIX ME").post().
                then();
    }
}
